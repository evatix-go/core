package coreversion

import "gitlab.com/evatix-go/core/constants"

const (
	VSymbol             = "v"
	InvalidVersionValue = constants.InvalidValue
)
