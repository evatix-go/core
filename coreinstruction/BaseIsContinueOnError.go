package coreinstruction

type BaseIsContinueOnError struct {
	IsContinueOnError bool `json:"IsContinueOnError"`
}
