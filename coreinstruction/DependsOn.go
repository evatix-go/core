package coreinstruction

type DependsOn struct {
	SpecificVersion
	DependencyName
	BaseIsLatest
}
