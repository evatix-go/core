package enumimpltests

import "gitlab.com/evatix-go/core/coreimpl/enumimpl"

type LeftRightDynamicMap struct {
	Left, Right enumimpl.DynamicMap
}
