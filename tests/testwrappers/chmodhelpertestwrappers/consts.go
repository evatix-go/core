package chmodhelpertestwrappers

import "gitlab.com/evatix-go/core/coretests"

const (
	RwxApplyOnPath                coretests.TestFuncName = "RwxApplyOnPath"
	UnixRwxApplyRecursivelyOnPath coretests.TestFuncName = "UnixRwxApplyRecursivelyOnPath"
)
