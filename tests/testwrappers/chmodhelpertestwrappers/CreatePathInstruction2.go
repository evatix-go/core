package chmodhelpertestwrappers

import "gitlab.com/evatix-go/core/chmodhelper"

var CreatePathInstruction2 = []*chmodhelper.DirFilesWithRwxPermission{
	{
		DirWithFiles: chmodhelper.DirWithFiles{
			Dir: "/temp/core/test-cases",
			Files: []string{
				"file-1.txt",
				"file-2.txt",
				"file-3.txt",
			},
		},
		ApplyRwx: DefaultRwx,
	},
	{
		DirWithFiles: chmodhelper.DirWithFiles{
			Dir: "/temp/core/test-cases-2",
			Files: []string{
				"file-1.txt",
				"file-2.txt",
				"file-3.txt",
			},
		},
		ApplyRwx: DefaultRwx,
	},
	{
		DirWithFiles: chmodhelper.DirWithFiles{
			Dir: "/temp/core/test-cases-3",
			Files: []string{
				"file-1.txt",
				"file-2.txt",
				"file-3.txt",
			},
		},
		ApplyRwx: DefaultRwx,
	},
}
