package coreinterface

type BooleanDisposer interface {
	Dispose() (isSuccess bool)
}
