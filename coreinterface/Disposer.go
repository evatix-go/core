package coreinterface

type Disposer interface {
	Dispose()
}
