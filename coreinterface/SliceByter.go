package coreinterface

type SliceByter interface {
	Bytes() []byte
}
