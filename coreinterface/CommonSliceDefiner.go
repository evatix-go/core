package coreinterface

import "gitlab.com/evatix-go/core/internal/internalinterface"

type CommonSliceDefiner interface {
	internalinterface.CommonSliceDefiner
}
