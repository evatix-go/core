package baseactioninf

type MustDefaultsInjector interface {
	MustInjectDefaults()
}
