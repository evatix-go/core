package baseactioninf

type ServiceNameGetter interface {
	ServiceName() string
}
