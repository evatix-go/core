package baseactioninf

type MustExecutor interface {
	MustExecute()
}
