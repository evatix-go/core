package coreinterface

type ByteIdentifier interface {
	IdByte() byte
}
