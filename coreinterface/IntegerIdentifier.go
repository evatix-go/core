package coreinterface

import "gitlab.com/evatix-go/core/internal/internalinterface"

type IntegerIdentifier interface {
	internalinterface.IntegerIdGetter
}
