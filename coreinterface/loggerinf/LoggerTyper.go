package loggerinf

import "gitlab.com/evatix-go/core/internal/internalinterface"

type LoggerTyper interface {
	internalinterface.LoggerTyper
}
