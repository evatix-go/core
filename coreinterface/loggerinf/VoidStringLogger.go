package loggerinf

type VoidStringLogger interface {
	Log(message string)
}
