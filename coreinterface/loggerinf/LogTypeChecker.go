package loggerinf

import "gitlab.com/evatix-go/core/internal/internalinterface"

type LogTypeChecker interface {
	internalinterface.LogTypeChecker
}
