package loggerinf

type EntityTypeNamer interface {
	EntityTypeName() string
}
