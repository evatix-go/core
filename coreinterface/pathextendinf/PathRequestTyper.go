package pathextendinf

import "gitlab.com/evatix-go/core/internal/internalinterface/internalpathextender"

type PathRequestTyper interface {
	internalpathextender.PathRequestTyper
}
