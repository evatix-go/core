package pathextendinf

import "gitlab.com/evatix-go/core/internal/internalinterface/internalpathextender"

type FileListerTyper interface {
	internalpathextender.FileListerTyper
}
