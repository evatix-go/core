package pathextendinf

import "gitlab.com/evatix-go/core/internal/internalinterface/internalpathextender"

type Identifier interface {
	internalpathextender.Identifier
}
