package enuminf

import "gitlab.com/evatix-go/core/internal/internalinterface"

type SimpleEnumer interface {
	internalinterface.SimpleEnumer
}
