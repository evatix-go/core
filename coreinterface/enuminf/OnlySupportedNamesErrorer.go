package enuminf

import "gitlab.com/evatix-go/core/internal/internalinterface/internalenuminf"

type OnlySupportedNamesErrorer interface {
	internalenuminf.OnlySupportedNamesErrorer
}
