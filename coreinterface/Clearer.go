package coreinterface

type Clearer interface {
	Clear()
}
