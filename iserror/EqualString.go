package iserror

func EqualString(left, right string) bool {
	return left == right
}
