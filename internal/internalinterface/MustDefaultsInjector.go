package internalinterface

type MustDefaultsInjector interface {
	InjectDefaultsMust()
}
