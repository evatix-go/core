package internalinterface

type ByteEnumValuer interface {
	ByteValue() byte
}
