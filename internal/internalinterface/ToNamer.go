package internalinterface

type ToNamer interface {
	Name() string
}
