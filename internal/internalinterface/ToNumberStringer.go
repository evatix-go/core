package internalinterface

type ToNumberStringer interface {
	ToNumberString() string
}
