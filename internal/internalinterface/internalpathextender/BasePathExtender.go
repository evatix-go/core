package internalpathextender

type BasePathExtender interface {
	Identifier
	PathInfoer
}
