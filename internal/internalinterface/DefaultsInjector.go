package internalinterface

type DefaultsInjector interface {
	InjectDefaults() error
}
