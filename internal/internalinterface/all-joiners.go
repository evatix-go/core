package internalinterface

type StringUsingJoiner interface {
	StringUsingJoiner(joiner string) string
}
