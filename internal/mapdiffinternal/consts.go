package mapdiffinternal

var (
	curlyWrapFormat                     = "{\n\n%s\n\n}"                          // jsonValueString
	diffBetweenMapShouldBeMessageFormat = "%s\n\nDifference Between Map:\n\n{%s}" // title, diff string
)
