package corestr

type CollectionsOfCollectionPtrModel struct {
	Items []*CollectionPtr `json:"PointerStringsCollectionsOfCollection"`
}
