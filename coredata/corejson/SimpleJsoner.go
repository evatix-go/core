package corejson

type SimpleJsoner interface {
	Jsoner
	JsonParseSelfInjector
}
