package corejson

type KeyWithJsoner struct {
	Key    string
	Jsoner Jsoner
}
