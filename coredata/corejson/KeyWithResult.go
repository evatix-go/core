package corejson

type KeyWithResult struct {
	Key    string
	Result Result
}
