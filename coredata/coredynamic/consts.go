package coredynamic

import "gitlab.com/evatix-go/core/constants"

const (
	supportedTypesMessageReference = " Supported Types: https://t.ly/1Lpt, "
	defaultMaxLevelOfReflection    = constants.DefaultMaxLevel
)
