package corerange

import "gitlab.com/evatix-go/core/constants"

type MinMaxInt8 struct {
	Min, Max int8
}

func (it *MinMaxInt8) CreateMinMaxInt() *MinMaxInt {
	return &MinMaxInt{
		Min: int(it.Min),
		Max: int(it.Min),
	}
}

func (it *MinMaxInt8) CreateRangeInt(rawString, separator string) *RangeInt {
	return NewRangeInt(
		rawString,
		separator,
		it.CreateMinMaxInt())
}

func (it *MinMaxInt8) CreateRangeInt8(rawString, separator string) *RangeInt8 {
	return NewRangeInt(
		rawString,
		separator,
		it.CreateMinMaxInt()).
		CreateRangeInt8()
}

func (it *MinMaxInt8) CreateRangeInt16(rawString, separator string) *RangeInt16 {
	return NewRangeInt(
		rawString,
		separator,
		it.CreateMinMaxInt()).
		CreateRangeInt16()
}

func (it *MinMaxInt8) Difference() int8 {
	return it.Max - it.Min
}

func (it *MinMaxInt8) DifferenceAbsolute() int8 {
	diff := it.Difference()

	if diff < 0 {
		return diff
	}

	return diff
}

func (it *MinMaxInt8) IsMinEqual(val int8) bool {
	return it != nil && it.Min == val
}

func (it *MinMaxInt8) IsMinAboveEqual(val int8) bool {
	return it != nil && it.Min >= val
}

func (it *MinMaxInt8) IsMinAbove(val int8) bool {
	return it != nil && it.Min > val
}

func (it *MinMaxInt8) IsMinLess(val int8) bool {
	return it != nil && it.Min < val
}

func (it *MinMaxInt8) IsMinLessEqual(val int8) bool {
	return it != nil && it.Min <= val
}

func (it *MinMaxInt8) IsMaxEqual(val int8) bool {
	return it != nil && it.Max == val
}

func (it *MinMaxInt8) IsMaxAboveEqual(val int8) bool {
	return it != nil && it.Max >= val
}

func (it *MinMaxInt8) IsMaxAbove(val int8) bool {
	return it != nil && it.Max > val
}

func (it *MinMaxInt8) IsMaxLess(val int8) bool {
	return it != nil && it.Max < val
}

func (it *MinMaxInt8) IsMaxLessEqual(val int8) bool {
	return it != nil && it.Max <= val
}

func (it *MinMaxInt8) RangeLengthInt() int {
	return int(it.RangeLength())
}

// RangeLength (5 - 3 = 2) + 1
func (it *MinMaxInt8) RangeLength() int8 {
	return it.DifferenceAbsolute() + 1
}

// RangesInt
//
//  returns empty integers if IsInvalid
//  return range int values
func (it *MinMaxInt8) RangesInt() []int {
	actualRanges := it.Ranges()
	rangesIntegers := make(
		[]int,
		it.RangeLengthInt())

	for i, actualValue := range actualRanges {
		rangesIntegers[i] = int(actualValue)
	}

	return rangesIntegers
}

// Ranges
//
//  returns empty integers if IsInvalid
//  return range int values
func (it *MinMaxInt8) Ranges() []int8 {
	length := it.RangeLength()
	start := it.Min
	slice := make([]int8, constants.Zero, length)
	var i int8

	for i = 0; i < length; i++ {
		slice[i] = start + i
	}

	return slice
}

// IsWithinRange r.Min >= value && value <= r.Max
func (it *MinMaxInt8) IsWithinRange(value int8) bool {
	return it.Min >= value && value <= it.Max
}

// IsInvalidValue  !r.IsWithinRange(value)
func (it *MinMaxInt8) IsInvalidValue(value int8) bool {
	return !it.IsWithinRange(value)
}

func (it MinMaxInt8) IsOutOfRange(value int8) bool {
	return !it.IsWithinRange(value)
}

func (it *MinMaxInt8) ClonePtr() *MinMaxInt8 {
	if it == nil {
		return nil
	}

	return &MinMaxInt8{
		Min: it.Min,
		Max: it.Max,
	}
}

func (it *MinMaxInt8) Clone() MinMaxInt8 {
	return MinMaxInt8{
		Min: it.Min,
		Max: it.Max,
	}
}

func (it *MinMaxInt8) IsEqual(right *MinMaxInt8) bool {
	if it == nil && right == nil {
		return true
	}

	if it == nil || right == nil {
		return true
	}

	if it == right {
		return true
	}

	return it.Max == right.Max &&
		it.Min == right.Min
}
