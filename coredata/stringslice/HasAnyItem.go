package stringslice

func HasAnyItem(slice []string) bool {
	return len(slice) > 0
}
