package stringslice

func MakeLen(length int) []string {
	return make([]string, length)
}
