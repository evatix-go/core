package testconsts

//goland:noinspection ALL
var (
	LowerCaseStringsArray = []string{"hello", "world", "one", "two"}
	UpperCaseStringsArray = []string{"HELLO", "WORLD", "ONE", "TWO"}
)
