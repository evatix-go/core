package coretaskinfo

const (
	infoFieldName          = "Name"
	infoFieldDescription   = "Description"
	infoFieldUrl           = "Url"
	infoFieldHintUrl       = "HintUrl"
	infoFieldErrorUrl      = "ErrorUrl"
	infoFieldExampleUrl    = "ExampleUrl"
	infoFieldSingleExample = "SingleExample"
	infoFieldExamples      = "Examples"
	payloadsField          = "Payloads"
	payloadsErrField       = "Payloads.SerializingErr"
)
