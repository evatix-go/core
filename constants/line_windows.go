package constants

const (
	// NewLine Reference: https://stackoverflow.com/a/49963413
	// Operating system based newline, for unix it is "\n"
	// For Windows Operating system, "\r\n"
	NewLine = NewLineWindows
)
