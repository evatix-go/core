package percentconst

const (
	OnePointTwoPercentIncrement = 1.2 // 1.2 multiplier
	FiftyPercentIncrement       = 1.5 // 1.5 multiplier
	SixtyPercentIncrement       = 1.6 // 1.6 multiplier
	SeventyPercentIncrement     = 1.7 // 1.7 multiplier
	DoubleIncrement             = 2   // 2 multiplier
	TwoFiftyPercentIncrement    = 2.5 // 2.5 multiplier
)
