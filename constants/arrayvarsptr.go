package constants

//goland:noinspection ALL
var (
	AsciiSpacePtr            = &AsciiSpace
	AsciiNewLinesCharsPtr    = &AsciiNewLinesChars
	EmptyStringsPtr          = &EmptyStrings
	EmptyPtrStringsPtr       = &EmptyPtrStrings
	EmptyIntsPtr             = &EmptyInts
	EmptyBytesPtr            = &EmptyBytes
	EmptyFloatsPtr           = &EmptyFloats
	EmptyFloat64sPtr         = &EmptyFloat64s
	EmptyInterfacesPtr       = &EmptyInterfaces
	EmptyIntToPtrIntsMapPtr  = &EmptyIntToPtrIntsMap
	EmptyIntToIntsMapPtr     = &EmptyIntToIntsMap
	EmptyIntToBytesMapPtr    = &EmptyIntToBytesMap
	EmptyIntToPtrBytesMapPtr = &EmptyIntToPtrBytesMap
	EmptyStringMapPtr        = &EmptyStringMap
	EmptyStrToIntsMapPtr     = &EmptyStrToIntsMap
	EmptyStrToPtrIntsMapPtr  = &EmptyStrToPtrIntsMap
	EmptyStrToPtrBytesMapPtr = &EmptyStrToPtrBytesMap
	EmptyStringsMapPtr       = &EmptyStringsMap
	EmptyPtrStringsMapPtr    = &EmptyPtrStringsMap
)
