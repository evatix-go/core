package bitsize

const (
	Of8      = 8
	Of16     = 16
	Of32     = 32
	Of64     = 64
	Of128    = 128
	Of256    = 256
	Of512    = 512
	Of1024   = 1024
	Of2048   = 2048
	Of4096   = 4096
	Of8192   = 8192
	Of16384  = 16384
	Of32768  = 32768
	Of65536  = 65536
	Of131072 = 131072
	Of1G     = 1024       // 1024, 1G
	Of2G     = 2048       // 2048, 2G
	Of4G     = 4096       // 4096, 4G
	Of8G     = 8192       // 8192, 8G
	Of16G    = 16384      // 16384, 16G
	Of32G    = 32768      // 32768, 32G
	Of64G    = 65536      // 65536, 64G
	Of128G   = 131072     // 131072, 128G
	Of256G   = Of128G * 2 // 256G
	Of512G   = Of256G * 2 // 512G
)
