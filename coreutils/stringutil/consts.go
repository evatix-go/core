package stringutil

import "gitlab.com/evatix-go/core/constants"

const (
	ExpectedLeftRightLength = constants.Two
)
