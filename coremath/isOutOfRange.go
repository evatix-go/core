package coremath

type isOutOfRange struct {
	Integer   integerOutOfRange
	Integer64 integer64OutOfRange
}
